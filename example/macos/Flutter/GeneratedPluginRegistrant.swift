//
//  Generated file. Do not edit.
//

import FlutterMacOS
import Foundation

import s10s_card_scanner

func RegisterGeneratedPlugins(registry: FlutterPluginRegistry) {
  S10sCardScannerPlugin.register(with: registry.registrar(forPlugin: "S10sCardScannerPlugin"))
}
