
import 'dart:async';

import 'package:flutter/services.dart';

class S10sCardScanner {
  static const MethodChannel _channel = MethodChannel('s10s/cardScanner');

  static Future<String?> get cardScanInfo async {
    final version = await _channel.invokeMethod('scanCard');
    return version;
  }
}
