#import "S10sCardScannerPlugin.h"
#if __has_include(<s10s_card_scanner/s10s_card_scanner-Swift.h>)
#import <s10s_card_scanner/s10s_card_scanner-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "s10s_card_scanner-Swift.h"
#endif

@implementation S10sCardScannerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftS10sCardScannerPlugin registerWithRegistrar:registrar];
}
@end
