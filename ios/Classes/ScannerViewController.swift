
import Foundation
import UIKit
import PayCardsRecognizer

final class ScannerViewController: UIViewController, PayCardsRecognizerPlatformDelegate {

    var scanned: (([String: String]) -> ())?

    lazy var recognizer: PayCardsRecognizer = .init(delegate: self, resultMode: .async, container: self.view, frameColor: .green)

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        recognizer.startCamera()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        recognizer.stopCamera()
    }

    func payCardsRecognizer(_ payCardsRecognizer: PayCardsRecognizer, didRecognize result: PayCardsRecognizerResult) {
        print("rec")
        var results: [String: String] = [:]
        results["cardNumber"] = result.recognizedNumber
        results["cardHolderName"] = result.recognizedHolderName
        if let expireDateMonth = result.recognizedExpireDateMonth, let expireDateYear = result.recognizedExpireDateYear {
            results["expirationDate"] = "\(expireDateMonth)/\(expireDateYear)"
        }
        scanned?(results)
    }
}
