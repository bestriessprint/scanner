import Flutter
import Foundation
import UIKit

public class SwiftS10sCardScannerPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "s10s/cardScanner", binaryMessenger: registrar.messenger())
    let instance = SwiftS10sCardScannerPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    if(call.method == "scanCard"){
        let scannerViewController = ScannerViewController()
        scannerViewController.scanned = { (data) -> () in
            scannerViewController.dismiss(animated: true)
            result(data)
        }
        scannerViewController.modalPresentationStyle = .popover
        let viewController = UIApplication.shared.keyWindow?.rootViewController
        scannerViewController.modalPresentationStyle = .popover
        viewController?.present(scannerViewController, animated: true)

        return
    }
  }
}
