package com.example.s10s_card_scanner

import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.android.FlutterActivity
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import android.content.Intent
import cards.pay.paycardsrecognizer.sdk.Card
import cards.pay.paycardsrecognizer.sdk.ScanCardIntent

import android.app.Activity
import android.content.Context

/** S10sCardScannerPlugin */
class S10sCardScannerPlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private var channelResult: MethodChannel.Result? = null
  private val REQUEST_CODE_SCAN_CARD = 1
  private lateinit var activity : Activity
  private lateinit var context: Context

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "s10s/cardScanner")
    channel.setMethodCallHandler(this)
    context = flutterPluginBinding.applicationContext
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    print("check")
    if (call.method == "scanCard") {
      val intent: Intent = ScanCardIntent.Builder(context).build()
      activity.startActivityForResult(intent, REQUEST_CODE_SCAN_CARD)
      print("salam popolam")
    } else {
      result.notImplemented()
    }
  }

//  override fun onDestroy() {
//    activity = null
//  }

  fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    if (requestCode == REQUEST_CODE_SCAN_CARD) {
      if (resultCode == Activity.RESULT_OK) {
        val card: Card? = data?.getParcelableExtra(ScanCardIntent.RESULT_PAYCARDS_CARD)
        val map = hashMapOf<String, String?>().apply {
          this["cardNumber"] = card?.cardNumber
          this["expirationDate"] = card?.expirationDate
          this["cardHolderName"] = card?.cardHolderName
        }
          channelResult?.success(map)
      }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
